import json
import os
import traceback

import shotgun_api3


CONFIG_FILENAME = "config.json"
CONFIG_PATH = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                           CONFIG_FILENAME)


def lambda_handler(event, context):
    """Sample pure Lambda function

    Parameters
    ----------
    event: dict, required
        API Gateway Lambda Proxy Input Format

        Event doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html#api-gateway-simple-proxy-for-lambda-input-format

    context: object, required
        Lambda Context runtime methods and attributes

        Context doc: https://docs.aws.amazon.com/lambda/latest/dg/python-context-object.html

    Returns
    ------
    API Gateway Lambda Proxy Output Format: dict

        Return doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html
    """

    if event["httpMethod"] != "POST":
        return {
            "statusCode": 500,
            "body": json.dumps({
                "message": "Only POST supported.",
            }),
        }

    try:
        event['body'] = json.loads(event['body'])
        sg = get_sg_connection(event['headers']['x-sg-webhook-site-url'])

        event_data = event['body']['data']
        sg.set_session_uuid(event_data['session_uuid'])

        version = sg.find_one(
            "Version",
            [["id", "is", event_data['entity']["id"]]],
            ["sg_status_list", "flagged"]
        )
        if version:
            success, flagged = toggle_flagged(sg, )
            if success:
                msg = ("Set Version #{} (status: '{}') -> 'Flagged' to {}"
                       .format(version["id"],
                               version["sg_status_list"],
                               flagged))
            else:
                msg = ("Version #{} (status: '{}') -> : 'Flagged' unchanged"
                       .format(version["id"], version["sg_status_list"]))

            return {
                "statusCode": 200,
                "body": msg
            }

        else:
            return {
                "statusCode": 200,
                "body": "Version #{} doesn't exist; Ignoring"
                        .format(event_data['entity']["id"])
            }

    except Exception as err:
        return {
            "statusCode": 500,
            "body": "{}\n\n{}".format(
                traceback.format_exc(),
                json.dumps(event)
            )
        }


def toggle_flagged(sg, version):
    """

    :param sg:
    :param version:
    :return:
    """
    if version["sg_status_list"] == "rvew" and not version["flagged"]:
        sg.update("Version", version["id"], {"flagged": True})
        return True, True
    elif version["sg_status_list"] != "rvew" and version["flagged"]:
        sg.update("Version", version["id"], {"flagged": False})
        return True, False

    return False, None


def get_sg_connection(site):
    if os.path.exists(CONFIG_PATH):
        with open(CONFIG_PATH) as config_file:
            config = json.load(config_file)
            return shotgun_api3.Shotgun(**config[site])

    raise RuntimeError("Unable to load config file from {}"
                       .format(CONFIG_PATH))
